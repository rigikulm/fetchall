# fetchall
“Fetches URLs in parallel and reports their times and sizes.”

    This code is based on the similarly named program from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

## Sample Programs

**fetchall usage**

``` bash
$ fetchall <url> [url2 url3 ...]
$
$ fetchall http://www.adobe.com http://www.amazon.com http://netflix.com
```

